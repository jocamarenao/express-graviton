#!/bin/sh
echo '-------------------------------configuring pre-commit-----------------------------------'
rm .git/hooks/pre-commit
echo '-------------------------------deleting api npm modules-------------------------------'
rm -rf node_modules
rm package-lock.json
echo '-------------------------------api npm modules done deleting--------------------------'
echo '-------------------------------deleting database------------------------------------------'
rm sequelizeData.json
psql -U $USER -d postgres -c  "SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname = 'celium'"
psql -U $USER -d postgres -c  "drop database celium"
echo '-------------------------------database deleted-------------------------------------------'