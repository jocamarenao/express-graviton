# Codeville Celium API Application

Your starting point for any CMS software project.

## Built With

* [Express](https://expressjs.com/) - Fast, unopinionated, minimalist web framework for Node.js
* [PostgreSQL](https://www.postgresql.org/) - The World's Most Advanced Open Source Relational Database
* [Sequelize](https://sequelize.org/) - Sequelize is a promise-based Node.js ORM for Postgres, MySQL, MariaDB, SQLite and Microsoft SQL Server.
* [Redis](https://redis.io/) - Redis is an open source (BSD licensed), in-memory data structure store, used as a database, cache and message broker.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

## Prerequisites

* node >= 10.16.3
* npm >= 5.9.0
* npx >= 6.9.0
* postgresSQL >= 11.5
* redis >= 5.0.5
* sequelize-cli >= 5.5.0
* jscpd >= latest

## Installing redis

### ubuntu

* https://tecadmin.net/install-redis-ubuntu/

## Installing npx

* https://www.npmjs.com/package/npx

## Installing jscpd (make sure is globally)
* https://github.com/kucherenko/jscpd#installation

## Installing postgres

### ubuntu
* https://www.digitalocean.com/community/tutorials/how-to-install-and-use-postgresql-on-ubuntu-18-04

## Post postgres installation

* Create a user with the same username as your sessions user (echo $USER) and set a password.
* https://www.a2hosting.com/kb/developer-corner/postgresql/managing-postgresql-databases-and-users-from-the-command-line

## Installing Project

In order to run the project you must follow the next steps:

* Copy .env.example to .env and the following changes in the .env file at the root of the project:

Replace the follow lines with the values of the database that you just created:
```
DB_USERNAME=
DB_PASSWORD=

```

* Execute the installation command (from the root of the project) 
```
source install.sh
```

* This command will:
* Clone project
* Install npm dependancies
* Create the database.
* Run Migrations (crating tables) and seeders (populating data).
* Making the project fully functional.

* Execute the start command (from the root of the project) 
```
npm run start
```

## Uninstalling
If you want to uninstall everything executed by the install.sh command, you can do it using by typing the following command at the root of the project.
```
source uninstall.sh 
```

You will be asked for confirmation before uninstalling the project.

## Quality Assurance tools
In order to keep the code clean, structured and based on certain standards, the following list of tools are implemented.


* [JSCPD](https://github.com/kucherenko/jscpd) - Copy/paste detector for programming source code.
* [Prettier](https://prettier.io/) -An opinionated code formatter Supports many languages Integrates with most editors Has few options
* [ESLint](https://eslint.org/) -An opinionated code formatter Supports many languages Integrates with most editors Has few options


## Reporting

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](). 

## Authors

* **Jonatan Camarena** - [jonatancamarena](https://gitlab.com/jocamarenao)

