const i18n = require('i18n');

i18n.configure({
	locales: ['en', 'es'],
	directory: './src/translations',
	defaultLocale: 'en',
	api: {
		__: 'trans',
	},
});

module.exports = async (req, res, next) => {
	await i18n.init(req, res);
	return next();
};
