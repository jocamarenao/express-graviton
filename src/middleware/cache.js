// libraries
const redis = require('redis');
// src
const { Setting } = require('@src/models');

module.exports = async function loadCache(req, res, next) {
	try {
		const client = await redis.createClient(process.env.REDIS_URL);
		const settings = await Setting.findAll();

		await client.get(settings[0].key, async (err, result) => {
			if (err) next(err);
			if (!result) {
				// eslint-disable-next-line no-restricted-syntax
				for (const item of settings) {
					// eslint-disable-next-line no-await-in-loop
					await client.set(item.key, item.value);
				}
			}
		});
		next();
	} catch (err) {
		next(err);
	}
};
