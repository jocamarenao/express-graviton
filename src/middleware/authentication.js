// Libraries
const jwt = require('jsonwebtoken');
// src
const { User } = require('@src/models');
const common = require('@src/helpers/common');
const { unauthorized } = require('@src/settings/httpStatus');

const authentication = async (req, res, next) => {
	try {
		const token = req.header('Authorization').replace('Bearer ', '');

		const decoded = jwt.verify(token, process.env.JWT_SECRET);

		const user = await User.findOne({
			where: {
				id: decoded.id,
				token,
			},
		});

		if (!user) return common.handleError(next, unauthorized);

		req.user = user;
		req.token = token;
		return next();
	} catch (e) {
		return common.handleError(next, unauthorized);
	}
};

module.exports = authentication;
