// libraries
const { validationResult } = require('express-validator/check');
// src
const common = require('@src/helpers/common');

const validationHasErrors = async (req, res, next) => {
	try {
		const validation = await validationResult(req);
		if (!validation.isEmpty()) return common.validateRequest(req, next);
		return next();
	} catch (err) {
		return next(err);
	}
};

module.exports = validationHasErrors;
