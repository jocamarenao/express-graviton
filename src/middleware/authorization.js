// src
const common = require('@src/helpers/common');
const acl = require('@src/helpers/acl');
const { unauthorized } = require('@src/settings/httpStatus');

const authorization = permission => {
	// add 1 in settings file
	return async (req, res, next) => {
		try {
			// eslint-disable-next-line prefer-destructuring
			req.query.resource = req.path.replace('/', '');
			const can = await acl.can(req.user.id, permission);
			if (!can) return common.handleError(next, unauthorized);
			return next();
		} catch (err) {
			return next(err);
		}
	};
};

module.exports = authorization;
