// libraries
const sgMail = require('@sendgrid/mail');
// src

module.exports = async function sendEmail(email) {
	try {
		const { to, subject, text } = email;
		sgMail.setApiKey(process.env.SENDGRID_API_KEY);
		await sgMail.send({
			to,
			from: process.env.EMAIL,
			subject,
			text,
		});
	} catch (err) {
		throw new Error(err);
	}
};
