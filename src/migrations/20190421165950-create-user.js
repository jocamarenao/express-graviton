// src
const { roles, statuses } = require('../settings/users');

module.exports = {
	// eslint-disable-next-line max-lines-per-function
	up: (queryInterface, Sequelize) => {
		return queryInterface.createTable('Users', {
			id: {
				allowNull: false,
				autoIncrement: true,
				primaryKey: true,
				type: Sequelize.INTEGER,
			},
			firstName: {
				type: Sequelize.STRING,
				allowNull: false,
			},
			middleName: {
				type: Sequelize.STRING,
				allowNull: true,
			},
			lastName: {
				type: Sequelize.STRING,
				allowNull: false,
			},
			email: {
				type: Sequelize.STRING,
				allowNull: false,
				unique: true,
			},
			username: {
				type: Sequelize.STRING,
				allowNull: false,
				unique: true,
			},
			role: {
				type: Sequelize.ENUM,
				values: roles,
			},
			status: {
				type: Sequelize.ENUM,
				values: statuses,
			},
			password: {
				type: Sequelize.STRING,
				allowNull: false,
			},
			resetPasswordToken: {
				type: Sequelize.STRING,
				allowNull: true,
			},
			resetPasswordTokenExpiresAt: {
				type: Sequelize.STRING,
				allowNull: true,
			},
			token: {
				type: Sequelize.STRING,
				allowNull: true,
			},
			tokenExpiresAt: {
				type: Sequelize.STRING,
				allowNull: true,
			},
			createdAt: {
				allowNull: false,
				type: Sequelize.DATE,
			},
			updatedAt: {
				allowNull: true,
				type: Sequelize.DATE,
			},
		});
	},
	down: queryInterface => {
		return queryInterface.dropTable('Users');
	},
};
