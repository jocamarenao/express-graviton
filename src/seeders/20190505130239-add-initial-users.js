// libraries
require('module-alias/register');
const moment = require('moment');
const { User } = require('@src/models');

module.exports = {
	up: () => {
		return User.bulkCreate(
			[
				{
					firstName: 'jonatan',
					lastName: 'camarena',
					username: 'camarena',
					email: 'camarena.dev@gmail.com',
					password: '123123',
					role: 'admin',
					createdAt: moment.utc().format(),
					updatedAt: moment.utc().format(),
				},
				{
					firstName: 'maricela',
					lastName: 'camarena',
					username: 'maricela',
					email: 'maricela@gmail.com',
					password: '123123',
					role: 'admin',
					createdAt: moment.utc().format(),
					updatedAt: moment.utc().format(),
				},
				{
					firstName: 'valerie',
					lastName: 'camarena',
					username: 'valerie',
					email: 'valerie@gmail.com',
					password: '123123',
					role: 'general',
					createdAt: moment.utc().format(),
					updatedAt: moment.utc().format(),
				},
				{
					firstName: 'victor',
					lastName: 'camarena',
					username: 'victor',
					email: 'victor@gmail.com',
					password: '123123',
					role: 'general',
					createdAt: moment.utc().format(),
					updatedAt: moment.utc().format(),
				},
			],
			{
				individualHooks: true,
			}
		);
	},

	down: () => {
		return User.destroy({ where: { username: ['camarena', 'maricela', 'valerie', 'victor'] }, individualHooks: true });
	},
};
