// libraries
const moment = require('moment');

module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.bulkInsert(
			'Settings',
			[
				{
					key: 'limitPublicRequests',
					value: '200',
					createdAt: moment.utc().format(),
					updatedAt: moment.utc().format(),
				},
				{
					key: 'limitPrivateRequests',
					value: '50',
					createdAt: moment.utc().format(),
					updatedAt: moment.utc().format(),
				},
				{
					key: 'recordsPerPage',
					value: '10',
					createdAt: moment.utc().format(),
					updatedAt: moment.utc().format(),
				},
				{
					key: 'recordsPerPageLarge',
					value: '100',
					createdAt: moment.utc().format(),
					updatedAt: moment.utc().format(),
				},
				{
					key: 'tokensExpiresIn',
					value: '180',
					createdAt: moment.utc().format(),
					updatedAt: moment.utc().format(),
				},
				{
					key: 'limitRequestsMilliseconds',
					value: '60000',
					createdAt: moment.utc().format(),
					updatedAt: moment.utc().format(),
				},
			],
			{}
		);
	},

	down: (queryInterface, Sequelize) => {
		return queryInterface.bulkDelete('Settings', null, {});
	},
};
