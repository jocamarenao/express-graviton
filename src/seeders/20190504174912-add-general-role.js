require('module-alias/register');
const acl = require('@src/helpers/acl');

module.exports = {
	up: async () => {
		await acl.createRole('general');
	},

	down: async () => {
		await acl.deleteRole('general');
	},
};
