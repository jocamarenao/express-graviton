require('module-alias/register');
const acl = require('@src/helpers/acl');

module.exports = {
	up: async () => {
		const parent = await acl.createPermission('users view', 'users_view', null);
		await acl.createPermission('users create', 'users_create', parent.id);
		await acl.createPermission('users update', 'users_update', parent.id);
		await acl.createPermission('users delete', 'users_delete', parent.id);
		await acl.createRole('admin');
		const permissions = ['users view', 'users create', 'users update', 'users delete'];
		await acl.givePermissionTo(permissions, 'admin');
	},

	down: async () => {
		const permissions = ['users view', 'users create', 'users update', 'users delete'];
		await acl.removePermissionTo(permissions, 'admin');
		await acl.deleteRole('admin');
		await acl.deletePermission('users view');
		await acl.deletePermission('users create');
		await acl.deletePermission('users update');
		await acl.deletePermission('users delete');
	},
};
