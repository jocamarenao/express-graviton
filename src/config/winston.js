// libraries
const winston = require('winston');

const options = {
	errorFile: {
		level: 'error',
		filename: 'src/logs/errors.log',
		handleExceptions: true,
		json: true,
		maxsize: 5242880, // 5MB
		maxFiles: 5,
		colorize: false,
	},
	infoFile: {
		level: 'info',
		filename: 'src/logs/info.log',
		handleExceptions: true,
		json: true,
		maxsize: 5242880, // 5MB
		maxFiles: 5,
		colorize: false,
	},
	console: {
		level: 'info',
		handleExceptions: true,
		json: false,
		colorize: true,
	},
};

const logger = winston.createLogger({
	transports: [new winston.transports.Console(options.console), new winston.transports.File(options.errorFile), new winston.transports.File(options.infoFile)],
	exitOnError: false,
});

logger.stream = {
	write: function write(message, encoding) {
		// logger.info(message);
	},
};

module.exports = logger;
