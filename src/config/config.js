require('dotenv').config();

module.exports = {
	development: {
		username: process.env.DB_USERNAME,
		password: process.env.DB_PASSWORD,
		logging: false,
		seederStorage: 'json',
		seederStoragePath: 'sequelizeData.json',
		seederStorageTableName: 'sequelize_data',
		database: process.env.DB_DATABASE,
		host: process.env.DB_HOST,
		port: process.env.DB_PORT,
		dialect: process.env.DB_DIALECT,
	},
	test: {
		username: 'root',
		password: null,
		logging: false,
		seederStorage: 'json',
		seederStoragePath: 'sequelizeData.json',
		seederStorageTableName: 'sequelize_data',
		database: 'database_test',
		host: '127.0.0.1',
		dialect: process.env.DB_DIALECT,
	},
	production: {
		use_env_variable: 'DATABASE_URL',
		dialect: 'postgres',
		logging: false,
		dialectOptions: {
			ssl: true,
		},
	},
};
