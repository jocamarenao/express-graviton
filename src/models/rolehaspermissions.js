module.exports = (sequelize, DataTypes) => {
	const RoleHasPermissions = sequelize.define(
		'RoleHasPermissions',
		{
			permissionId: DataTypes.INTEGER,
			roleId: DataTypes.INTEGER,
		},
		{}
	);
	RoleHasPermissions.associate = function associate(models) {
		RoleHasPermissions.hasMany(models.Roles, { sourceKey: 'roleId', foreignKey: 'id', as: 'RoleHasPermissionsRoles' });
		RoleHasPermissions.hasMany(models.Permissions, {
			sourceKey: 'permissionId',
			foreignKey: 'id',
			as: 'RoleHasPermissionsPermissions',
		});
	};
	return RoleHasPermissions;
};
