module.exports = (sequelize, DataTypes) => {
	const Roles = sequelize.define(
		'Roles',
		{
			name: DataTypes.STRING,
		},
		{}
	);
	Roles.associate = function associate(models) {
		Roles.belongsTo(models.RoleHasPermissions, { targetKey: 'roleId', foreignKey: 'id', as: 'permissionsRole' });
		Roles.belongsTo(models.UserHasPermissions, { targetKey: 'userId', foreignKey: 'id', as: 'permissionsUser' });
	};
	return Roles;
};
