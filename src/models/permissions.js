module.exports = (sequelize, DataTypes) => {
	const Permissions = sequelize.define(
		'Permissions',
		{
			name: DataTypes.STRING,
			codeName: DataTypes.STRING,
			parentId: DataTypes.INTEGER,
		},
		{}
	);
	Permissions.associate = function associate(models) {
		Permissions.belongsTo(models.RoleHasPermissions, { targetKey: 'roleId', foreignKey: 'id', as: 'rolePermissions' });
		Permissions.belongsTo(models.UserHasPermissions, { targetKey: 'userId', foreignKey: 'id', as: 'permissionsUsers' });
	};
	return Permissions;
};
