// libraries
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const moment = require('moment');

// src
const { roles, statuses } = require('@src/settings/users');
const {
	hashPassword: { saltRounds },
	hoursBeforeTokenExpires,
} = require('@src/settings/common');

// eslint-disable-next-line max-lines-per-function
module.exports = (sequelize, DataTypes) => {
	const User = sequelize.define(
		'User',
		{
			firstName: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			middleName: {
				type: DataTypes.STRING,
				allowNull: true,
			},
			lastName: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			email: {
				type: DataTypes.STRING,
				allowNull: false,
				unique: true,
			},
			username: {
				type: DataTypes.STRING,
				allowNull: false,
				unique: true,
			},
			role: {
				type: DataTypes.STRING,
				allowNull: false,
				in: roles,
			},
			status: {
				type: DataTypes.STRING,
				allowNull: false,
				defaultValue: 'active',
				in: statuses,
			},
			password: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			resetPasswordToken: {
				type: DataTypes.STRING,
				allowNull: true,
			},
			resetPasswordTokenExpiresAt: {
				type: DataTypes.DATE,
				allowNull: true,
			},
			token: {
				type: DataTypes.STRING,
				allowNull: true,
			},
			tokenExpiresAt: {
				type: DataTypes.DATE,
				allowNull: true,
			},
		},
		{
			scopes: {
				protected: {
					attributes: { exclude: ['password', 'resetPasswordToken', 'resetPasswordTokenExpiresAt', 'token'] },
					order: [['createdAt', 'DESC']],
				},
				defaultScope: {
					order: [['createdAt', 'DESC']],
				},
			},
		}
	);

	User.prototype.generateAuthToken = async function generateAuthToken() {
		const token = jwt.sign({ id: this.id }, process.env.JWT_SECRET);
		const tokenExpiresAt = moment.utc().add(hoursBeforeTokenExpires, 'hours');
		this.update(
			{
				token,
				tokenExpiresAt,
			},
			{
				hooks: false,
			}
		);
		return {
			token,
			tokenExpiresAt,
		};
	};

	User.prototype.fullname = function getFullname() {
		let fullname = this.firstName;
		if (this.middleName) fullname += ` ${this.middleName}`;
		fullname += ` ${this.lastName}`;
		return fullname;
	};

	User.prototype.getModulePermissions = async function getModulePermissions() {
		const permissions = {};
		// eslint-disable-next-line global-require
		const acl = require('@src/helpers/acl');
		const userPermissions = await acl.getAllPermissions(this.id, this.role);
		userPermissions.forEach(permission => {
			const modulePermission = permission.name.split(' ');
			// eslint-disable-next-line no-prototype-builtins
			if (!permissions.hasOwnProperty(modulePermission[0])) {
				permissions[modulePermission[0]] = [];
			}
			permissions[modulePermission[0]].push(modulePermission[1]);
		});
		return permissions;
	};

	User.authenticate = async (email, password) => {
		try {
			const user = await User.findOne({
				where: {
					email,
				},
			});
			if (!user) return false;

			const isPasswordValid = await bcrypt.compare(password, user.password);
			if (!isPasswordValid) return false;

			return user;
		} catch (err) {
			throw new Error(err);
		}
	};

	User.beforeCreate(async user => {
		try {
			// eslint-disable-next-line no-param-reassign
			user.password = await bcrypt.hash(user.password, saltRounds);
		} catch (err) {
			throw new Error(err);
		}
	});

	User.beforeUpdate(async user => {
		try {
			// eslint-disable-next-line no-param-reassign
			if (user.password) user.password = await bcrypt.hash(user.password, saltRounds);
		} catch (err) {
			throw new Error(err);
		}
	});

	User.beforeDestroy(async user => {
		try {
			// eslint-disable-next-line global-require
			const acl = require('@src/helpers/acl');
			await acl.removeRole(user.id, user.role);
		} catch (err) {
			throw new Error(err);
		}
	});

	User.afterCreate(async user => {
		try {
			// eslint-disable-next-line global-require
			const acl = require('@src/helpers/acl');
			await acl.assignRole(user.id, user.role);
		} catch (err) {
			throw new Error(err);
		}
	});

	User.associate = function associate(models) {
		User.belongsTo(models.RoleHasPermissions, { targetKey: 'roleId', foreignKey: 'id', as: 'rolePermissions' });
		User.belongsTo(models.UserHasPermissions, { targetKey: 'userId', foreignKey: 'id', as: 'userPermissions' });
	};

	return User;
};
