module.exports = (sequelize, DataTypes) => {
	const UserHasPermissions = sequelize.define(
		'UserHasPermissions',
		{
			permissionId: DataTypes.INTEGER,
			roleId: DataTypes.INTEGER,
			userId: DataTypes.INTEGER,
		},
		{}
	);
	UserHasPermissions.associate = function associate(models) {
		UserHasPermissions.hasMany(models.Roles, { sourceKey: 'roleId', foreignKey: 'id', as: 'UserHasPermissionsRoles' });
		UserHasPermissions.hasMany(models.User, { sourceKey: 'userId', foreignKey: 'id', as: 'UserHasPermissionsUsers' });
		UserHasPermissions.hasMany(models.Permissions, {
			sourceKey: 'permissionId',
			foreignKey: 'id',
			as: 'UserHasPermissionsPermissions',
		});
	};
	return UserHasPermissions;
};
