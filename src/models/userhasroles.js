module.exports = (sequelize, DataTypes) => {
	const UserHasRoles = sequelize.define(
		'UserHasRoles',
		{
			roleId: DataTypes.INTEGER,
			userId: DataTypes.INTEGER,
		},
		{}
	);
	UserHasRoles.associate = function associate(models) {
		UserHasRoles.hasMany(models.Roles, { sourceKey: 'roleId', foreignKey: 'id', as: 'UserHasRolesRoles' });
		UserHasRoles.hasMany(models.User, { sourceKey: 'userId', foreignKey: 'id', as: 'UserHasRolesUsers' });
	};
	return UserHasRoles;
};
