module.exports = (sequelize, DataTypes) => {
	const Setting = sequelize.define(
		'Setting',
		{
			key: DataTypes.STRING,
			value: DataTypes.STRING,
		},
		{}
	);
	return Setting;
};
