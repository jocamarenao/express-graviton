require('module-alias/register');
require('dotenv').config();
// libraries
const express = require('express');
const helmet = require('helmet');
const morgan = require('morgan');
const cors = require('cors');
// src
const i18n = require('@src/translations');
const winston = require('@src/config/winston');

const port = process.env.PORT;
const app = express();

app.use(cors());
app.use(express.json());
app.use(helmet());
app.use(i18n);
app.use(morgan('combined', { stream: winston.stream }));

app.get('/', (req, res) => {
	res.send({ msg: 'celium api is live.' });
});

require('./routes')(app);

// seperate
app.listen(port, () => {});
