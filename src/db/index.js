const Sequelize = require('sequelize');

let sequelize = null;

if (process.env.DATABASE_URL) {
	sequelize = new Sequelize(process.env.DATABASE_URL, {
		logging: false,
		dialect: 'postgres',
		dialectOptions: {
			ssl: true,
		},
	});
} else {
	sequelize = new Sequelize(process.env.DB_DATABASE, process.env.DB_USERNAME, process.env.DB_PASSWORD, {
		host: process.env.DB_HOST,
		dialect: process.env.DB_DIALECT,
		logging: false,
	});
}

module.exports = sequelize;
