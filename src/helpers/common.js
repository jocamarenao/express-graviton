// libraries
const moment = require('moment');
const httpStatus = require('http-status');
const Sequelize = require('sequelize');
const redis = require('redis');
const { promisify } = require('util');
const { validationResult } = require('express-validator/check');
// src
const { ranges, oneWeek, twoWeeks, oneDay, oneMonth, oneYear } = require('@src/settings/dates');
const settings = require('@src/settings/filters');
const { recordsPerPage, defaultPage, firstPage } = require('@src/settings/filters');

const { Op } = Sequelize;

class Common {
	static async validationErrors(errors) {
		return errors;
	}

	static catchErrors(fn) {
		return (req, res, next) => {
			return fn(req, res, next).catch(next);
		};
	}

	static async handleFilters(query, path) {
		const limit = recordsPerPage;
		const sorts = [];
		let order = [];
		// eslint-disable-next-line no-restricted-globals
		const page = !query.page || isNaN(query.page) || query.page < firstPage ? defaultPage : query.page;
		const { sort } = query;

		const offset = page * limit - limit;

		const options = {
			offset,
			limit,
			where: {},
		};

		if (sort) {
			sorts.push(sort.split(':')[0]);
			sorts.push(sort.split(':')[1]);
			order = [sorts];
		}

		Object.keys(query).forEach(item => {
			if (!['sort', 'query'].includes(item)) {
				if (settings[path].whitelist.includes(item)) {
					options.where[item] = this.getCondition(item, query[item], path);
				}
			}
		});

		if (query.range && ranges.default.includes(query.range)) {
			options.where.createdAt = this.buildDateFilter(query.range, query.startDate, query.endDate);
		}
		return { options, page, order };
	}

	// refactor
	static buildDateFilter(dateRange, startDate, endDate) {
		let range = [this.dateRange(dateRange).startDate, this.dateRange(dateRange).endDate];
		if (dateRange === 'custom') {
			const start = moment.utc(startDate).set({ hour: 0, minute: 0, second: 0, millisecond: 0 });
			const end = moment.utc(endDate).set({ hour: 23, minute: 59, second: 59, millisecond: 999 });

			if (startDate && endDate) {
				range = [start, end];
			} else if (startDate) {
				return { [Op.gte]: start };
			} else if (endDate) {
				return { [Op.lte]: end };
			}
		}
		return { [Op.between]: range };
	}

	// eslint-disable-next-line max-lines-per-function
	static dateRange(range) {
		let startDate = '';
		let endDate = '';
		switch (range) {
			case 'today':
				startDate = moment.utc().startOf('day');
				endDate = moment.utc().endOf('day');
				break;
			case 'yesterday':
				startDate = moment
					.utc()
					.subtract(oneDay, 'days')
					.startOf('day');
				endDate = moment
					.utc()
					.subtract(oneDay, 'days')
					.endOf('day');
				break;
			case 'current_week':
				startDate = moment.utc().startOf('week');
				endDate = moment.utc().endOf('week');
				break;
			case 'last_week':
				startDate = moment
					.utc()
					.subtract(oneWeek, 'week')
					.startOf('week');
				endDate = moment
					.utc()
					.subtract(oneWeek, 'week')
					.endOf('week');
				break;
			case 'last_two_weeks':
				startDate = moment
					.utc()
					.subtract(twoWeeks, 'week')
					.startOf('week');
				endDate = moment
					.utc()
					.subtract(oneWeek, 'week')
					.endOf('week');
				break;
			case 'current_month':
				startDate = moment.utc().startOf('month');
				endDate = moment.utc().endOf('month');
				break;
			case 'last_month':
				startDate = moment
					.utc()
					.subtract(oneMonth, 'month')
					.startOf('month');
				endDate = moment
					.utc()
					.subtract(oneMonth, 'month')
					.endOf('month');
				break;
			case 'current_year':
				startDate = moment.utc().startOf('year');
				endDate = moment.utc().endOf('year');
				break;
			default:
				startDate = moment
					.utc()
					.subtract(oneYear, 'year')
					.startOf('year');
				endDate = moment
					.utc()
					.subtract(oneYear, 'year')
					.endOf('year');
		}
		return { endDate, startDate };
	}

	static handleError(next, status, err) {
		const error = new Error();
		error.status = 500;
		error.message = httpStatus['500'];

		if (status !== null) {
			error.status = status;
			error.message = httpStatus[status];
		}

		if (err !== null && err !== undefined) {
			error.message = err;
		}

		if (next === null) throw error;

		return next(error);
	}

	static escapeRegex(text) {
		return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
	}

	static getCondition(key, value, path) {
		if (settings[path].conditions[key] === 'like') {
			return { [Op.like]: `%${value}%` };
		}
		return {};
	}

	static async cache(key) {
		const client = redis.createClient(process.env.REDIS_URL);
		const getAsync = promisify(client.get).bind(client);
		const cachedKey = await getAsync(key);
		return cachedKey;
	}

	static async validateRequest(req, next) {
		try {
			const validation = await validationResult(req);
			const error = new Error();
			const errors = [];

			validation.array().forEach(item => {
				if (errors.some(param => item.param === param.param)) {
					errors.forEach(err => {
						if (err.param === item.param) {
							// eslint-disable-next-line no-param-reassign
							if (item.msg.includes('is required')) err.errors = [];
							err.errors.push(item.msg);
						}
					});
				} else if (!errors.some(param => item.param === param)) {
					errors.push({
						param: item.param,
						errors: [item.msg],
					});
				}
			});

			error.status = 422;
			error.message = httpStatus['422'];
			error.errors = errors;

			return next(error);
		} catch (err) {
			const error = new Error();
			error.status = 500;
			error.message = err;
			return next(error);
		}
	}
}

module.exports = Common;
