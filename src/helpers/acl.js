/* eslint-disable no-await-in-loop */
/* eslint-disable no-return-await */
// src
const sequelize = require('@src/db');
const common = require('@src/helpers/common');
const { User, Permissions, RoleHasPermissions, Roles, UserHasPermissions, UserHasRoles } = require('@src/models');

class Acl {
	static async createPermission(name, codeName, parentId) {
		try {
			const permission = await Permissions.create({ name, codeName, parentId });
			return permission;
		} catch (err) {
			return common.handleError(null, null, err);
		}
	}

	static async deletePermission(codeName) {
		try {
			await Permissions.destroy({ where: { name: codeName } });
		} catch (err) {
			common.handleError(null, null, err);
		}
	}

	static async createRole(roleName) {
		try {
			const role = await Roles.create({ name: roleName });
			return role;
		} catch (err) {
			return common.handleError(null, null, err);
		}
	}

	static async deleteRole(roleName) {
		try {
			const role = await Roles.destroy({ where: { name: roleName } });
			return role;
		} catch (err) {
			return common.handleError(null, null, err);
		}
	}

	static async givePermissionTo(permissions, roleName) {
		return this.mutatePermission(permissions, roleName, 'assign');
	}

	static async removePermissionTo(permissions, roleName) {
		return this.mutatePermission(permissions, roleName, 'remove');
	}

	static async mutatePermission(permissions, roleName, type) {
		try {
			await sequelize.transaction(async t => {
				const role = await Roles.findOne({ where: { name: roleName } });
				// eslint-disable-next-line no-restricted-syntax
				for (const item of permissions) {
					const permission = await Permissions.findOne({ where: { name: item } });
					if (!permission || !role) common.handleError(null, null, null);
					if (type === 'assign') await RoleHasPermissions.create({ permissionId: permission.id, roleId: role.id });
					if (type === 'remove') await RoleHasPermissions.destroy({ where: { permissionId: permission.id, roleId: role.id } });
				}
			});
		} catch (err) {
			common.handleError(null, null, err);
		}
	}

	static async assignRole(userId, roleName) {
		return this.mutateRole(userId, roleName, 'assign');
	}

	static async removeRole(userId, roleName) {
		return this.mutateRole(userId, roleName, 'remove');
	}

	static async mutateRole(userId, roleName, type) {
		try {
			const role = await Roles.findOne({ where: { name: roleName } });
			const user = await User.findByPk(userId);
			if (!role || !user) return common.handleError(null, null, null);
			if (type === 'assign') return await UserHasRoles.create({ userId: user.id, roleId: role.id });
			return await UserHasRoles.destroy({ where: { userId: user.id, roleId: role.id } });
		} catch (err) {
			return common.handleError(null, null, err);
		}
	}

	static async can(userId, permissionName) {
		try {
			const user = await User.findByPk(userId);
			const permission = await Permissions.findOne({ where: { name: permissionName } });
			const role = await Roles.findOne({ where: { name: user.role } });
			if (!permission || !user || !role) return false;
			const userHasPermission = await UserHasPermissions.findOne({ where: { userId: user.id, permissionId: permission.id } });
			const roleHasPermission = await RoleHasPermissions.findOne({ where: { roleId: role.id, permissionId: permission.id } });
			if (userHasPermission || roleHasPermission) return true;
			return false;
		} catch (err) {
			return false;
		}
	}

	// refactor wait in for with promise all
	static async getAllPermissions(userId, roleName) {
		const role = await Roles.findOne({ where: { name: roleName } });
		const userHasPermissions = await UserHasPermissions.findAll({ where: { userId } });
		const roleHasPermissions = await RoleHasPermissions.findAll({ where: { roleId: role.id } });
		const permissions = [];
		// eslint-disable-next-line no-restricted-syntax
		for (const item of roleHasPermissions) {
			// eslint-disable-next-line no-await-in-loop
			const permission = await Permissions.findByPk(item.permissionId);
			permissions.push(permission);
		}
		// eslint-disable-next-line no-restricted-syntax
		for (const item of userHasPermissions) {
			// eslint-disable-next-line no-await-in-loop
			const permission = await Permissions.findByPk(item.permissionId);
			if (!permissions.some(permissionItem => permissionItem.id === permission.id)) permissions.push(permission);
		}

		return permissions;
	}
}

module.exports = Acl;
