// libraries
const { validationResult } = require('express-validator/check');

class CommonUtils {
	static async validationHasErrors(req) {
		const validation = await validationResult(req);
		return !validation.isEmpty();
	}
}

module.exports = CommonUtils;
