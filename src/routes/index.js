// Libraries
const rateLimit = require('express-rate-limit');
const httpStatus = require('http-status');
// src
const cache = require('@src/middleware/cache');
const common = require('@src/helpers/common');
const winston = require('@src/config/winston');
const { notFound, internalServerError, badRequest, toManyRequests } = require('@src/settings/httpStatus');
// modules
const userRoutes = require('@modules/User/UserRoutes');
const authorizationRoutes = require('@modules/Authorization/AuthorizationRoutes');
const authenticationRoutes = require('@modules/Authentication/AuthenticationRoutes');
const permissionRoutes = require('@modules/Permission/PermissionRoutes');

module.exports = async function loadRoutes(app) {
	const publicLimiter = rateLimit({
		windowMs: await common.cache('limitRequestsMilliseconds'),
		max: await common.cache('limitPublicRequests'),
		message: httpStatus[toManyRequests],
	});

	const privateLimiter = rateLimit({
		windowMs: await common.cache('limitRequestsMilliseconds'),
		max: await common.cache('limitPrivateRequests'),
		message: httpStatus[toManyRequests],
	});

	app.use('/api/v1', cache, publicLimiter, authenticationRoutes.guest, authorizationRoutes.guest);
	app.use('/api/v1', privateLimiter, userRoutes.auth, authenticationRoutes.auth, authorizationRoutes.auth, permissionRoutes.auth);

	app.use('*', (req, res, next) => common.handleError(next, notFound));

	// eslint-disable-next-line max-params
	app.use((err, req, res, next) => {
		const error = {
			status: internalServerError,
			message: err,
		};

		if (err.status) {
			error.status = err.status;
			error.message = err.message;
		}

		if (err.errors) error.errors = err.errors;

		if ([internalServerError, badRequest].includes(error.status)) winston.error(`${error.status} - ${error.message} - ${req.originalUrl} - ${req.method}`);
		res.status(error.status).json(error);
	});
};
