// src
const { User } = require('@src/models');

const env = process.env.APP_ENV;

const password = {
	in: ['body'],
	custom: {
		options: (value, { req }) => {
			if (typeof value === 'undefined' || !value) throw new Error('The password is required');
			if (value !== (req.body.confirmPassword || req.body.confirmNewPassword)) throw new Error('The password and confirm password dont match');
			return true;
		},
	},
};

if (env === 'production') {
	password.matches = {
		options: '^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$',
		errorMessage: 'The password must be more than 8 characters long, should contain at least 1 uppercase, 1 lowercase, 1 numeric and 1 special character.',
	};
}

const email = shouldEmailExist => {
	return {
		in: ['body'],
		isEmail: { errorMessage: 'The email must be a valid email address' },
		custom: {
			options: (value, row) => {
				if (typeof value === 'undefined' || !value) throw new Error('The email is required');
				return User.findOne({ where: { email: value } })
					.then(user => {
						if (shouldEmailExist && !user) {
							throw new Error('The email is invalid');
						} else if (!shouldEmailExist && user) {
							if (row.req.method === 'PATCH' && +row.req.params.user_id === user.id) return true;
							throw new Error('The email already exists');
						}
						return true;
					})
					.catch(() => {
						if (shouldEmailExist) {
							throw new Error('The email is invalid');
						} else if (!shouldEmailExist) {
							throw new Error('The email already exists');
						}
					});
			},
		},
	};
};

module.exports = { password, email };
