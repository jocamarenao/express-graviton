const success = 200;
const created = 201;
const badRequest = 400;
const unauthorized = 401;
const notFound = 404;
const toManyRequests = 429;
const internalServerError = 500;

module.exports = {
	success,
	created,
	badRequest,
	unauthorized,
	notFound,
	toManyRequests,
	internalServerError,
	successful: [success, created],
};
