module.exports = {
	ranges: {
		default: ['today', 'yesterday', 'current_week', 'last_week', 'last_two_weeks', 'current_month', 'last_month', 'current_year', 'custom'],
	},
	oneDay: 1,
	oneWeek: 1,
	twoWeeks: 2,
	oneMonth: 1,
	oneYear: 1,
};
