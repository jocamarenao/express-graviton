// src
const users = require('@modules/User/UserSettings').filters;

module.exports = {
	users,
	recordsPerPage: 10,
	defaultPage: 1,
	firstPage: 1,
};
