module.exports = {
	hashPassword: {
		convertToBase: 36,
		slice: -8,
		saltRounds: 8,
	},
	hoursBeforeTokenExpires: 3,
};
