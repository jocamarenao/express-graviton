// src
const { User } = require('@src/models');
const { success, unauthorized } = require('@src/settings/httpStatus');

class AuthenticationService {
	static async processLogin(email, password) {
		const user = await User.authenticate(email, password);
		if (!user) return { status: unauthorized };
		const { token, tokenExpiresAt } = await user.generateAuthToken();
		return { status: success, data: { token, tokenExpiresAt, tokenType: 'Bearer' } };
	}

	static async processLogout(user) {
		await user.update({
			token: null,
		});
		return { status: success, data: {} };
	}
}

module.exports = AuthenticationService;
