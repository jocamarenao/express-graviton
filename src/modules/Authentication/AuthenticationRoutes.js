// Libraries
const express = require('express');
const validationHasErrors = require('@src/middleware/validationHasErrors');
const { checkSchema } = require('express-validator/check');

// src
require('@src/db');
const authentication = require('@src/middleware/authentication');
const schemas = require('@modules/Authentication/schemas');
const commonHelpers = require('@src/helpers/common');
const authenticationController = require('@modules/Authentication/AuthenticationController');
//
const auth = new express.Router();
const guest = new express.Router();

guest.post('/authentication/login', checkSchema(schemas.login), validationHasErrors, commonHelpers.catchErrors(authenticationController.login));
auth.post('/authentication/logout', authentication, commonHelpers.catchErrors(authenticationController.logout));

module.exports = { auth, guest };
