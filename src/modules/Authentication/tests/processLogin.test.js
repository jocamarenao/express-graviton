// libraries
const chai = require('chai');
require('module-alias/register');
// src
const authenticationService = require('@modules/Authentication/AuthenticationService');
const { success, unauthorized } = require('@src/settings/httpStatus');
//
const { expect } = chai;

describe('Authentication Service', function authenticationServiceTest() {
	describe('Login user', function loginUser() {
		it('when user credentials are correct, then the user should be signed in successfully', async function onSuccess() {
			const user = {
				email: 'camarena.dev@gmail.com',
				password: '123123',
			};
			const processLoginResponse = await authenticationService.processLogin(user.email, user.password);
			expect(processLoginResponse.status).equal(success);
		});
		it('when user credentials are incorrect, sign in should be unsuccessful', async function onError() {
			const user = {
				email: 'camarena.dev@gmail.com',
				password: '123',
			};
			const processLoginResponse = await authenticationService.processLogin(user.email, user.password);
			expect(processLoginResponse.status).equal(unauthorized);
		});
	});
});
