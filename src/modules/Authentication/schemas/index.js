// src
const { email, password } = require('./rules');

module.exports = {
	login: {
		email,
		password,
	},
};
