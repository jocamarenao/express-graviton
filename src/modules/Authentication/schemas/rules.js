const email = {
	in: ['body'],
	custom: {
		options: emailValue => {
			if (typeof emailValue === 'undefined' || !emailValue) throw new Error('The email is required');
			return true;
		},
	},
};

const password = {
	in: ['body'],
	custom: {
		options: passwordValue => {
			if (typeof passwordValue === 'undefined' || !passwordValue) throw new Error('The password is required');
			return true;
		},
	},
};

module.exports = { email, password };
