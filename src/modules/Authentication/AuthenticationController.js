// Src
const authenticationService = require('@modules/Authentication/AuthenticationService');
const common = require('@src/helpers/common');
const { successful } = require('@src/settings/httpStatus');

class AuthenticationController {
	static async login(req, res, next) {
		const processLoginResponse = await authenticationService.processLogin(req.body.email, req.body.password);
		if (!successful.includes(processLoginResponse.status)) return common.handleError(next, processLoginResponse.status);
		return res.send(processLoginResponse);
	}

	static async logout(req, res, next) {
		const processLogoutResponse = await authenticationService.processLogout(req.user);
		if (!successful.includes(processLogoutResponse.status)) return common.handleError(next, processLogoutResponse.status);
		return res.send();
	}
}

module.exports = AuthenticationController;
