const { firstName, lastName, username, role, password, email } = require('./rules');

module.exports = {
	post: {
		firstName,
		lastName,
		username,
		role,
		password,
		email,
	},
	patch: {
		firstName,
		lastName,
		username,
		role,
		email,
	},
};
