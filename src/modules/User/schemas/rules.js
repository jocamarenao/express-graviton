// src
const { User, Roles } = require('@src/models');
const { password } = require('@src/settings/rules');
const email = require('@src/settings/rules').email(false);

const firstName = {
	in: ['body'],
	isAlphanumeric: { errorMessage: 'The firstName should contain numbers or letters' },
	isLength: {
		options: { min: 3, max: 30 },
		errorMessage: 'The firstName should be between 3 and  30 characters long',
	},
	custom: {
		options: value => {
			if (typeof value === 'undefined' || !value) throw new Error('The firstName is required');
			return true;
		},
	},
};
const lastName = {
	in: ['body'],
	isAlphanumeric: { errorMessage: 'The lastName should contain numbers or letters' },
	isLength: {
		options: { min: 3, max: 30 },
		errorMessage: 'The lastName should be between 3 and  30 characters long',
	},
	custom: {
		options: value => {
			if (typeof value === 'undefined' || !value) throw new Error('The lastName is required');
			return true;
		},
	},
};
const username = {
	in: ['body'],
	isAlphanumeric: { errorMessage: 'The username should contain numbers or letters' },
	isLength: {
		options: { min: 5, max: 30 },
		errorMessage: 'The username should be between 5 and 30 characters long',
	},
	custom: {
		options: (value, row) => {
			if (typeof value === 'undefined' || !value) throw new Error('The username is required');
			return User.findOne({ where: { username: value } })
				.then(user => {
					if (user) {
						if (row.req.method === 'PATCH' && +row.req.params.user_id === user.id) return true;
						throw new Error('The username already exists');
					}
					return true;
				})
				.catch(() => {
					throw new Error('The username already exists');
				});
		},
	},
};
const role = {
	in: ['body'],
	custom: {
		options: (name, row) => {
			if (typeof name === 'undefined' || !name) throw new Error('The role is required');
			return Roles.findOne({ where: { name } })
				.then(user => {
					if (!user) {
						throw new Error('The role is invalid');
					}
				})
				.catch(() => {
					throw new Error('The role is invalid');
				});
		},
	},
};

module.exports = { firstName, lastName, username, role, password, email };
