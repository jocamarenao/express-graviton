// libraries
const express = require('express');
const validationHasErrors = require('@src/middleware/validationHasErrors');
const { checkSchema } = require('express-validator/check');
// src
const authorization = require('@src/middleware/authorization');
const authenticate = require('@src/middleware/authentication');
const schemas = require('@modules/User/schemas');
const userController = require('@modules/User/UserController');
const commonHelpers = require('@src/helpers/common');
//
const auth = new express.Router();

auth.get('/users', authenticate, authorization('users view'), commonHelpers.catchErrors(userController.index));
auth.get('/users/:userId', authenticate, authorization('users view'), commonHelpers.catchErrors(userController.show));
auth.post('/users', authenticate, authorization('users create'), checkSchema(schemas.post), validationHasErrors, commonHelpers.catchErrors(userController.create));
auth.delete('/users/:userId', authenticate, authorization('users delete'), commonHelpers.catchErrors(userController.delete));
auth.patch('/users/:userId', authenticate, authorization('users update'), checkSchema(schemas.patch), validationHasErrors, commonHelpers.catchErrors(userController.update));

module.exports = { auth };
