// libraries
require('module-alias/register');
const chai = require('chai');
// src
const userService = require('@modules/User/UserService');
const { success } = require('@src/settings/httpStatus');
const { User } = require('@src/models');
//
const { expect } = chai;

module.exports = function deleteUser() {
	it('user should be deleted successfully', async function onSuccess() {
		const user = await User.scope('protected').findOne({ where: { firstName: 'firstName2', email: 'username2@gmail.com' } });
		const deleteUserResponse = await userService.deleteUser(user.id);
		expect(deleteUserResponse.status).equal(success);
	});
};
