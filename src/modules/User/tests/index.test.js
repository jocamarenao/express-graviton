/* eslint-disable */
// src
const { createUserSuccessfully, createUserUnsuccessfully } = require('./createUser.js');
const updateUser = require('./updateUser.js');
const deleteUser = require('./deleteUser.js');

describe('User Service', function userServiceTest() {
	describe('create user with correct params', createUserSuccessfully.bind(this));
	describe('update user with correct params', updateUser.bind(this));
	describe('delete user with correct user id', deleteUser.bind(this));
});
