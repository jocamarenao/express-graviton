// libraries
require('module-alias/register');
const chai = require('chai');
const moment = require('moment');
// src
const userService = require('@modules/User/UserService');
const { success } = require('@src/settings/httpStatus');
const { User } = require('@src/models');
//
const { expect } = chai;

module.exports = function updateUser() {
	it('user should be updated successfully', async function onSuccess() {
		const user = await User.scope('protected').findOne({ where: { firstName: 'firstName', email: 'username@gmail.com' } });
		const updatedUser = {
			firstName: 'firstName2',
			lastName: 'lastName2',
			username: 'username2',
			email: 'username2@gmail.com',
			createdAt: moment.utc().format(),
			updatedAt: moment.utc().format(),
		};
		const updateUserResponse = await userService.updateUser(updatedUser, user.dataValues.id);
		expect(updateUserResponse.status).equal(success);
	});
};
