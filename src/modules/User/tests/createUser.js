// libraries
require('module-alias/register');
const chai = require('chai');
const moment = require('moment');
// src
const userService = require('@modules/User/UserService');
const { created } = require('@src/settings/httpStatus');
//
const { expect } = chai;
module.exports = {
	createUserSuccessfully: function createUserSuccessfully() {
		it('user should be created successfully', async function onSuccess() {
			const user = {
				firstName: 'firstName',
				lastName: 'lastName',
				username: 'username',
				email: 'username@gmail.com',
				password: '123123',
				role: 'admin',
				createdAt: moment.utc().format(),
				updatedAt: moment.utc().format(),
			};
			const createUserResponse = await userService.createUser(user);
			expect(createUserResponse.status).equal(created);
		});
	},
};
