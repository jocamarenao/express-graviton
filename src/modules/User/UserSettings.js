module.exports = {
	statuses: {
		default: ['active', 'suspended'],
		active: ['active'],
		cantLogin: ['suspended'],
		valid: ['active'],
		invalid: ['suspended'],
	},
	roles: {
		all: ['admin', 'general'],
		default: ['admin'],
		admins: ['admin'],
		general: ['general'],
	},
	filters: {
		whitelist: ['firstName', 'username'],
		conditions: { firstName: 'like', username: 'like' },
	},
	update: {
		whitelist: ['firstName', 'lastName', 'username', 'role', 'email'],
	},
};
