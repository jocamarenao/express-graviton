// src
const updateWhitelist = require('@modules/User/UserSettings').update.whitelist;
const { success, notFound, created } = require('@src/settings/httpStatus');
const common = require('@src/helpers/common');
const { User } = require('@src/models');

class AuthenticationService {
	static async getUser(userId) {
		const user = await User.scope('protected').findByPk(userId);
		if (!user) return { status: notFound };
		return { status: success, data: user };
	}

	static async getUsers(query, path) {
		const { options, page, order } = await common.handleFilters(query, path);
		const users = await User.scope('protected').findAll({ ...options, order });
		if (!users) return { status: notFound };
		return { status: success, data: users, meta: { page } };
	}

	static async createUser(payload) {
		await User.create(payload);
		return { status: created };
	}

	static async updateUser(payload, userId) {
		const updatedUser = {};
		const user = await User.scope('protected').findByPk(userId);
		if (!user) return { status: notFound };
		Object.keys(payload).forEach(item => {
			if (updateWhitelist.includes(item)) updatedUser[item] = payload[item];
		});
		await user.update(updatedUser);
		return { status: success };
	}

	static async deleteUser(userId) {
		await User.destroy({ where: { id: userId } });
		return { status: success };
	}
}

module.exports = AuthenticationService;
