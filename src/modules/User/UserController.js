// src
const { successful } = require('@src/settings/httpStatus');
const common = require('@src/helpers/common');
const userService = require('@modules/User/UserService');

class UserController {
	static async index(req, res, next) {
		const getUsersResponse = await userService.getUsers(req.query, req.path.replace('/', ''));
		if (!successful.includes(getUsersResponse.status)) return common.handleError(next, getUsersResponse.status);
		return res.send(getUsersResponse);
	}

	static async show(req, res, next) {
		const getUserResponse = await userService.getUser(req.params.userId);
		if (!successful.includes(getUserResponse.status)) return common.handleError(next, getUserResponse.status);
		return res.send(getUserResponse);
	}

	static async create(req, res, next) {
		const createUserResponse = await userService.createUser(req.body);
		if (!successful.includes(createUserResponse.status)) return common.handleError(next, createUserResponse.status);
		return res.status(createUserResponse.status).send();
	}

	static async delete(req, res, next) {
		const deleteUserResponse = await userService.deleteUser(req.params.userId);
		if (!successful.includes(deleteUserResponse.status)) return common.handleError(next, deleteUserResponse.status);
		return res.send();
	}

	static async update(req, res, next) {
		const updateUserResponse = await userService.updateUser(req.body, req.params.userId);
		if (!successful.includes(updateUserResponse.status)) return common.handleError(next, updateUserResponse.status);
		return res.send();
	}
}

module.exports = UserController;
