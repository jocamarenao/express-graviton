// src
require('@src/db');
const common = require('@src/helpers/common');
const permissionService = require('@modules/Permission/PermissionService');
const { successful } = require('@src/settings/httpStatus');

class PermissionController {
	static async permissions(req, res, next) {
		const { user, params } = req;
		const { id, role } = user;
		const { userId } = params;

		const processPermissionsResponse = await permissionService.processPermissions({ id, role }, userId);
		if (!successful.includes(processPermissionsResponse.status)) return common.handleError(next, processPermissionsResponse.status);
		return res.send(processPermissionsResponse);
	}
}
module.exports = PermissionController;
