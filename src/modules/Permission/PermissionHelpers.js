class PermissionHelpers {
	static cleanPermissions(permissions) {
		const response = [];
		permissions.forEach(item => {
			response.push({
				id: item.id,
				name: item.name,
				codeName: item.codeName,
				parentId: item.parentId,
			});
		});
		return response;
	}

	static buildPermissions(permissions, parentId) {
		const response = [];
		permissions.forEach(item => {
			if (item.parentId === parentId) {
				const permission = {
					id: item.id,
					name: item.name,
					codeName: item.codeName,
				};
				const children = this.buildPermissions(permissions, item.id);

				if (children) {
					permission.children = children;
				}

				response.push(permission);
			}
		});
		return response;
	}
}

module.exports = PermissionHelpers;
