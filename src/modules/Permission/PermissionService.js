// src
const permissionHelpers = require('@modules/Permission/PermissionHelpers');
const { notFound, success } = require('@src/settings/httpStatus');
const { User } = require('@src/models');
const acl = require('@src/helpers/acl');

class PermissionService {
	static async processPermissions(session, userId) {
		let permissions = [];

		const currentUser = {
			id: session.id,
			role: session.role,
		};

		if (userId) {
			const user = await User.findByPk(userId);
			if (!user)
				return {
					status: notFound,
					error: {},
				};
			currentUser.id = user.id;
			currentUser.role = user.role;
		}

		// use cache to store all permissions
		// eslint-disable-next-line global-require
		permissions = await acl.getAllPermissions(currentUser.id, currentUser.role);
		permissions = permissionHelpers.cleanPermissions(permissions);
		permissions = permissionHelpers.buildPermissions(permissions, null);
		return {
			status: success,
			data: {
				permissions,
			},
		};
	}
}

module.exports = PermissionService;
