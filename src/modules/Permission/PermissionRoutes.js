// libraries
const express = require('express');
// src
const authentication = require('@src/middleware/authentication');
const commonHelpers = require('@src/helpers/common');
const permissionController = require('./PermissionController');

const auth = new express.Router();

auth.get('/permissions', authentication, commonHelpers.catchErrors(permissionController.permissions));
auth.get('/permissions/users/:userId', authentication, commonHelpers.catchErrors(permissionController.permissions));

module.exports = { auth };
