// libraries
const moment = require('moment');
const jwt = require('jsonwebtoken');
const redis = require('redis');
const bcrypt = require('bcryptjs');
const { promisify } = require('util');
// src
const { User } = require('@src/models');
const { unauthorized, notFound, success } = require('@src/settings/httpStatus');
const { hashPassword } = require('@src/settings/common');

class AuthorizationHelpers {
	static async getTokensUser(token) {
		const decoded = jwt.verify(token, process.env.JWT_SECRET);
		const now = moment.utc();

		const user = await User.findOne({
			where: {
				id: decoded.id,
				resetPasswordToken: token,
			},
		});

		if (!user || user.resetPasswordToken_expires_at > now)
			return {
				status: unauthorized,
				error: {},
			};

		return {
			status: success,
			data: { user },
		};
	}

	static async getCurrentUser(sessionUserId, userId) {
		const response = { status: notFound, error: {} };
		const sessionUser = await User.scope('protected').findByPk(sessionUserId);
		if (!sessionUser) return response;

		let user = sessionUser;

		if (userId) {
			const bodyUser = await User.scope('protected').findByPk(userId);
			if (!sessionUser) return response;

			if (bodyUser.role === 'admin' || sessionUser.role === 'general') {
				response.status = unauthorized;
				return response;
			}

			user = bodyUser;
		}

		return {
			status: success,
			data: {
				user,
			},
		};
	}

	static async generateUsersPasswordToken(email) {
		const client = redis.createClient(process.env.REDIS_URL);
		const getAsync = promisify(client.get).bind(client);
		const tokenExpiresIn = await getAsync('tokensExpiresIn');

		const generateUsersPassword = await bcrypt.hash(
			Math.random()
				.toString(hashPassword.convertToBase)
				.slice(hashPassword.slice),
			hashPassword.saltRounds
		);

		const user = await User.findOne({ where: { email } });
		if (!user) return { status: notFound, error: {} };

		return {
			status: success,
			data: {
				user,
				token: jwt.sign({ id: user.id, token: generateUsersPassword }, process.env.JWT_SECRET),
				tokenExpiresIn: moment.utc().add(tokenExpiresIn, 'days'),
			},
		};
	}
}

module.exports = AuthorizationHelpers;
