/* eslint-disable mocha/no-top-level-hooks */
/* eslint-disable mocha/no-hooks-for-single-case */
// libraries
require('module-alias/register');
const chai = require('chai');
const AuthorizationHelpers = require('@modules/Authorization/AuthorizationHelpers');
const { User } = require('@src/models');

// src
const { success, unauthorized } = require('@src/settings/httpStatus');
const Op = require('@src/config/op');
//
const { $not } = Op;
const { expect } = chai;

module.exports = {
	getCurrentUser: function getCurrentUser() {
		let sessionId;
		let adminUserId;
		let generalUserId;

		before(async function beforeTests() {
			const session = await User.findOne({ where: { role: 'admin' } });
			sessionId = session.dataValues.id;
			const adminUser = await User.findOne({ where: { role: 'admin', id: { [$not]: sessionId } } });
			const generalUser = await User.findOne({ where: { role: 'general' } });
			adminUserId = adminUser.dataValues.id;
			generalUserId = generalUser.dataValues.id;
		});

		describe('multiple tests on getCurrentUser', function tests() {
			describe('a admin is updating he`s own password', function admin() {
				it('should return a status = 200', async function onSuccess() {
					const getCurrentUserResponse = await AuthorizationHelpers.getCurrentUser(sessionId);
					expect(getCurrentUserResponse.status).equal(success);
				});
				it('should contain a user object with an id property the equals the admins id', async function onSuccess() {
					const getCurrentUserResponse = await AuthorizationHelpers.getCurrentUser(sessionId);
					expect(getCurrentUserResponse.data.user.dataValues.id).equal(sessionId);
				});
			});
			describe('a admin is updating a general role user', function adminUpdatingGeneral() {
				it('should return a status = 200', async function onSuccess() {
					const getCurrentUserResponse = await AuthorizationHelpers.getCurrentUser(sessionId, generalUserId);
					expect(getCurrentUserResponse.status).equal(success);
				});
				it('should contain a user object with an id property the equals the general roles id', async function onSuccess() {
					const getCurrentUserResponse = await AuthorizationHelpers.getCurrentUser(sessionId, generalUserId);
					expect(getCurrentUserResponse.data.user.dataValues.id).equal(generalUserId);
				});
			});
			describe('a admin is updating a admin role user', function adminUpdatingGeneral() {
				it('should return a status = 401', async function onSuccess() {
					const getCurrentUserResponse = await AuthorizationHelpers.getCurrentUser(sessionId, adminUserId);
					expect(getCurrentUserResponse.status).equal(unauthorized);
				});
			});
		});
	},
};
