const { email, token, password, resetPassword } = require('./rules');

module.exports = {
	forgotPassword: {
		email,
	},
	resetForgotPassword: {
		token,
		password,
	},
	resetPassword: {
		currentPassword: resetPassword,
		newPassword: password,
	},
};
