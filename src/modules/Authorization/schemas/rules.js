// libraries
const bcrypt = require('bcryptjs');
// src
const { User } = require('@src/models');
const { password } = require('@src/settings/rules');
const email = require('@src/settings/rules').email(true);

const token = {
	in: ['body'],
	isJWT: { errorMessage: 'The token must be a valid token' },
	custom: {
		options: value => {
			if (typeof value === 'undefined' || !value) throw new Error('The token is required');
			return true;
		},
	},
};

const resetPassword = {
	in: ['body'],
	custom: {
		options: (value, { req }) => {
			if (typeof value === 'undefined' || !value) throw new Error('The password is required');
			return User.findOne({ where: { id: req.user.id } })
				.then(async user => {
					if (!user) throw new Error('The password is invalid');
					const isPasswordValid = await bcrypt.compare(value, user.password);
					if (!isPasswordValid) throw new Error('The password is invalid');
					return true;
				})
				.catch(() => {
					throw new Error('The password is invalid');
				});
		},
	},
};

module.exports = { email, token, password, resetPassword };
