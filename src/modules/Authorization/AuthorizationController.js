// src
require('@src/db');
const common = require('@src/helpers/common');
const authorizationService = require('@modules/Authorization/AuthorizationService');
const { successful } = require('@src/settings/httpStatus');

class AuthorizationController {
	static async fetchUser(req, res, next) {
		const processFetchUserResponse = await authorizationService.processFetchUser(req.user);
		if (!successful.includes(processFetchUserResponse.status)) return common.handleError(next, processFetchUserResponse.status);
		return res.send(processFetchUserResponse);
	}

	static async forgotPassword(req, res, next) {
		const {
			body: { email },
			trans,
		} = req;

		const translations = {
			line1: trans('reset_password_email_line1'),
			line2: trans('reset_password_email_line2'),
			line4: trans('reset_password_email_line4'),
			subject: trans('reset_password_email_subject'),
		};

		const processForgotPasswordResponse = await authorizationService.processForgotPassword(email, translations);
		if (!successful.includes(processForgotPasswordResponse.status)) return common.handleError(next, processForgotPasswordResponse.status);
		return res.send();
	}

	static async resetForgotPassword(req, res, next) {
		const {
			body: { token, password },
			trans,
		} = req;

		const processForgotResetPasswordResponse = await authorizationService.processResetForgotPassword(token, password, trans);
		if (!successful.includes(processForgotResetPasswordResponse.status)) return common.handleError(next, processForgotResetPasswordResponse.status);
		return res.send();
	}

	static async resetPassword(req, res, next) {
		const { user, body } = req;
		const { id } = user;
		const { userId, newPassword } = body;

		const processResetPasswordResponse = await authorizationService.processResetPassword(id, userId, newPassword);
		if (!successful.includes(processResetPasswordResponse.status)) return common.handleError(next, processResetPasswordResponse.status);
		return res.send();
	}
}

module.exports = AuthorizationController;
