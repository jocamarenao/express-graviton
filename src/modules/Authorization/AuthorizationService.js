// src
const AuthorizationHelpers = require('@modules/Authorization/AuthorizationHelpers');
const sendEmail = require('@src/emails/send_email');
const { successful, success } = require('@src/settings/httpStatus');

class AuthorizationService {
	static async processFetchUser(user) {
		const permissions = await user.getModulePermissions();
		return {
			status: success,
			data: {
				id: user.id,
				username: user.username,
				fullname: user.fullname(),
				email: user.email,
				role: user.role,
				permissions,
			},
		};
	}

	static async processForgotPassword(email, trans) {
		const generateUsersPasswordTokenResponse = await AuthorizationHelpers.generateUsersPasswordToken(email);
		if (!successful.includes(generateUsersPasswordTokenResponse.status)) return generateUsersPasswordTokenResponse;
		const {
			data: { token, user, tokensExpiresIn },
		} = generateUsersPasswordTokenResponse;

		await user.update({ resetPasswordToken: token, resetPasswordTokenExpiresAt: tokensExpiresIn });

		let text = `${trans.line1}\n\n`;
		text += `${trans.line2}\n\n`;
		text += `${process.env.APP_URL}${process.env.APP_PRE_FIX}/reset-password/${token}\n\n`;
		text += `${trans.line4}\n\n`;

		const sendEmailPayload = {
			to: email,
			subject: trans.subject,
			text,
		};

		await sendEmail(sendEmailPayload);

		return generateUsersPasswordTokenResponse;
	}

	static async processResetForgotPassword(token, password, trans) {
		const getTokensUserResponse = await AuthorizationHelpers.getTokensUser(token, password);

		if (!successful.includes(getTokensUserResponse.status)) return getTokensUserResponse;
		await getTokensUserResponse.data.user.update({ resetPasswordToken: null, resetPasswordTokenExpiresAt: null, password });

		let text = `${trans('reset_password_changed_line1')}\n\n`;
		text += `${trans('reset_password_changed_line2')}\n\n`;
		text += `${process.env.APP_URL}/#/login`;

		const emailPayload = {
			to: getTokensUserResponse.data.user.dataValues.email,
			subject: trans('reset_password_changed_subject'),
			text,
		};

		await sendEmail(emailPayload);

		return getTokensUserResponse;
	}

	static async processResetPassword(sessionUserId, userId, newPassword) {
		const getCurrentUserResponse = await AuthorizationHelpers.getCurrentUser(sessionUserId, userId);
		if (!successful.includes(getCurrentUserResponse.status)) return getCurrentUserResponse;
		await getCurrentUserResponse.data.user.update({ password: newPassword });
		return getCurrentUserResponse;
	}
}

module.exports = AuthorizationService;
