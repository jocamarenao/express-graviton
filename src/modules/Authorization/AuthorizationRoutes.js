// libraries
const express = require('express');
const { checkSchema } = require('express-validator/check');

// src
const schemas = require('@modules/Authorization/schemas');
const authentication = require('@src/middleware/authentication');
const validationHasErrors = require('@src/middleware/validationHasErrors');
const commonHelpers = require('@src/helpers/common');
const authorizationController = require('./AuthorizationController');

const auth = new express.Router();
const guest = new express.Router();

guest.patch('/authorization/forgotPassword', checkSchema(schemas.forgotPassword), validationHasErrors, commonHelpers.catchErrors(authorizationController.forgotPassword));
guest.patch('/authorization/resetForgotPassword', checkSchema(schemas.resetForgotPassword), validationHasErrors, commonHelpers.catchErrors(authorizationController.resetForgotPassword));

auth.patch('/authorization/resetPassword', authentication, checkSchema(schemas.resetPassword), validationHasErrors, commonHelpers.catchErrors(authorizationController.resetPassword));
auth.get('/authorization/user', authentication, commonHelpers.catchErrors(authorizationController.fetchUser));

module.exports = { auth, guest };
