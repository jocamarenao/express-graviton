#!/bin/sh
echo '-------------------------------configuring pre-commit-----------------------------------'
ln -s -f ../../pre-commit .git/hooks/pre-commit
chmod +x .git/hooks/pre-commit
echo '-------------------------------installing api npm modules-------------------------------'
npm install
echo '-------------------------------api npm modules done installing--------------------------'
echo '-------------------------------creating database----------------------------------------'
psql -U $USER -d postgres -c "create database celium"
echo '-------------------------------database created-----------------------------------------'
echo '-------------------------------migrating database---------------------------------------'
npx sequelize db:migrate
echo '-------------------------------database migrated----------------------------------------'
echo '-------------------------------seeding database-----------------------------------------'
npx sequelize db:seed:all
echo '-------------------------------database seeded------------------------------------------'